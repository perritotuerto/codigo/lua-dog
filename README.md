# Lua Dog

Adds functions to Lua Standard Libraries and Pandoc Library.

## Install

    luarocks install lua-dog

## Usage

You can use Lua Dog in 2 ways.

The laziest way is:

    local dog = require("dog")
    dog.import()
    -- Lua Dog 'dog.os.uname()' function was imported as 'os.uname()'
    os.uname()

This will import all Lua Dog functions to the Lua Standard Libraries and Pandoc Library.

If you prefer not to import, the way is:

    require "dog"
    -- Lua Dog 'dog.os.uname()' function was NOT imported as 'os.uname()'
    dog.os.uname()

## Functions

Check `src`.
